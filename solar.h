/*
 *  solar.h
 *  solar
 *
 *  Created by Samuel Hobl
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

// function prototypes
void init(int width, int height);
void resize(int width, int height);
void timer(int value);
void keyPressed(unsigned char key, int x, int y);
void keyReleased(unsigned char key, int x, int y);
void specialKeyPressed(int key, int x, int y);
void mouseMotion(int x, int y);
void camera();
void display(void);
void generateSkyBox(int pWidth, int pHeight, int pLength);
void setSkyBox(int pWidth, int pHeight, int pLength);
void createSphere(double r,int n, GLuint texture);
void initTexture(unsigned char* ptr, int imgWidth, int imgHeight, GLuint texId);

