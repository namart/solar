/*
 *  solar.c
 *  solar
 *
 *  Created by Samuel Hobl
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifdef __APPLE__
#include <GLUT/glut.h> 
#include <OpenGL/gl.h>  
#include <OpenGL/glu.h>  
#else
#include <GL/glut.h> 
#include <GL/gl.h>  
#include <GL/glu.h>  
#endif

#include <stdlib.h> // for exit
#include <unistd.h> // for usleep

#include "SOIL.h" // for imageLoading
#include "solar.h"

#include <math.h>
/* some math.h files don't define pi... */
#ifndef M_PI
#define M_PI 3.141592653
#endif

#define RAD(x) (((x)*M_PI)/180.)
#define PI 3.141592654
#define TWOPI 6.283185308
#define PID2 1.57079632679

#ifdef __STRICT_ANSI__
#define sinf(x) ((float)sin((x)))
#define cosf(x) ((float)cos((x)))
#define atan2f(x, y) ((float)atan2((x), (y)))
#endif 

//Point
typedef struct Point {
	float x;
	float y;
	float z;
} XYZ;

// window identifier
int window; 

//Time variables
float hour = 0.0;
float day = 0.0;
double inc = 0.00000277;
float cloud = 0.0;
float cloudspeed;

//Camera Settings
int rotationX = 0;
// rotation
float xpos = 0, ypos = 0, zpos = 0, xrot = 0, yrot = 0;
// mouse position
float lastx, lasty;

//textures
GLuint tex_skybox[5];
GLuint tex_planets[12];
GLuint tex_moons[10];

//listen
int list;

//Movement
int keyW = 0;
int keyA = 0;
int keyS = 0;
int keyD = 0;
//speed
int keyG = 0;


///////////////////////////
//          Main         //
///////////////////////////

int main(int argc, char **argv) {  
	glutInit(&argc, argv);  
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);  
	glutInitWindowSize(800, 600);  
	glutInitWindowPosition(0, 0);  
	window = glutCreateWindow("foo");  
	glutDisplayFunc(&display);  
	glutTimerFunc(10, &timer, 1); 
	glutReshapeFunc(&resize);
	glutSpecialFunc(&specialKeyPressed);
	glutPassiveMotionFunc(&mouseMotion);
	glutFullScreen();
	
	glutKeyboardFunc(&keyPressed);
	glutIgnoreKeyRepeat(1);
	glutKeyboardUpFunc(&keyReleased);

	init(640, 480);
	glutMainLoop();  
	return 0;
}

void init(int width, int height) {
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0);        
	glDepthFunc(GL_LESS);      
	glEnable(GL_DEPTH_TEST); 
	glShadeModel(GL_SMOOTH); 
	glEnable( GL_TEXTURE_2D );
	
	//Material
	GLfloat mat_shininess[] = { 1 };
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	
	//Light
    GLfloat light_ambient[] = { 0.0, 0.0, 0.0, 0.0 };
	GLfloat light_diffuse[] = { 0.0, 0.0, 0.0, 0.0 };	
	GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	
    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
	glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_specular);
	//glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, -2.0);
	
	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);

	resize(width, height);
	
	int imgWidth, imgHeight, imgChannels;
	unsigned char* ptr;
	
	//Textures for SkyBox
	glGenTextures(6, &tex_skybox[0]);
	
	ptr = SOIL_load_image("galaxy_b.bmp", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_skybox[0]);
	
	ptr = SOIL_load_image("galaxy_f.bmp", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_skybox[1]);
	
	ptr = SOIL_load_image("galaxy_d.bmp", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_skybox[2]);
	
	ptr = SOIL_load_image("galaxy_u.bmp", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_skybox[3]);
	
	ptr = SOIL_load_image("galaxy_l.bmp", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_skybox[4]);
	
	ptr = SOIL_load_image("galaxy_r.bmp", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_skybox[5]);
	
	//Textures for Plantes
	glGenTextures(10, &tex_planets[0]);
	
	ptr = SOIL_load_image("sun.png", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[0]);
	
	ptr = SOIL_load_image("merkur.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[1]);
	
	ptr = SOIL_load_image("venus.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[2]);
	
	ptr = SOIL_load_image("earth.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[3]);
	
	ptr = SOIL_load_image("cloud.png", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[4]);
	
	ptr = SOIL_load_image("moon.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[5]);
	
	ptr = SOIL_load_image("mars.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[6]);
	
	ptr = SOIL_load_image("jupiter.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[7]);
	
	ptr = SOIL_load_image("saturn.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[8]);
	
	ptr = SOIL_load_image("uranus.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[9]);
	
	ptr = SOIL_load_image("neptun.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[10]);
	
	ptr = SOIL_load_image("pluto.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_planets[11]);
	
	//Textures for moons
	ptr = SOIL_load_image("phobos.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_moons[0]);
	
	ptr = SOIL_load_image("deimos.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_moons[1]);
	
	ptr = SOIL_load_image("io.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_moons[2]);
	
	ptr = SOIL_load_image("europa.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_moons[3]);
	
	ptr = SOIL_load_image("ganymede.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_moons[4]);
	
	ptr = SOIL_load_image("callisto.jpg", &imgWidth, &imgHeight, &imgChannels, SOIL_LOAD_RGBA);
	initTexture(ptr, imgWidth, imgHeight, tex_moons[5]);
}

void initTexture(unsigned char* ptr, int imgWidth, int imgHeight, GLuint texId ) {
	if(ptr)
	{
		glBindTexture(GL_TEXTURE_2D, texId);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, 4, imgWidth, imgHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, ptr);
		SOIL_free_image_data(ptr);
	}
	else {
		printf("loading failed");
	}
}

//////////////////////////////
// Timer & Resize Callbacks //
//////////////////////////////

void resize(int width, int height) {
	// prevent division by zero
	if (height == 0) { height=1; }
	
	glViewport(0, 0, width, height);  
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (float)width/(float)height, 0.1f, 100000.0f);
	glMatrixMode(GL_MODELVIEW);
}

void timer(int value) {
	glutPostRedisplay();
	glutTimerFunc(10, &timer, 1);
}

///////////////////////////
//      Key Callbacks    //
///////////////////////////


void keyPressed(unsigned char key, int x, int y) {

	
	if(key == 'g') {
		keyG = 1;
	}

	if(key == 27) {
		glutDestroyWindow(window);
		exit(0);
	}

	// forward movement
	if(key == 'w') {
		keyW = 1;
	}
	
	// backward movement
	if(key == 's') {
		keyS = 1;
	}
	
	// leftward movement
	if(key == 'a') {
		keyA= 1;
	}
	
	// rightward movement
	if(key == 'd') {
		keyD = 1;
	}

	
}

void keyReleased(unsigned char key, int x, int y) {
	
	if(key == 'g') {
		keyG = 0;
	}
	
	// forward movement
	if(key == 'w') {
		keyW = 0;
	}
	
	// backward movement
	if(key == 's') {
		keyS = 0;
	}
	
	// leftward movement
	if(key == 'a') {
		keyA= 0;
	}
	
	// rightward movement
	if(key == 'd') {
		keyD = 0;
	}
	
}

void specialKeyPressed(int key, int x, int y) {

	switch (key) {
		case GLUT_KEY_UP:    
			inc *= 5.5;
			break;
		case GLUT_KEY_DOWN:
			inc /= 5.5;
			break;
	}
	
	if (inc < 0.00000277) {
		inc = 0.00000277;
	} else if (inc > 1000) {
		inc = 1000;
	}

}


///////////////////////////
//     Mouse Callbacks   //
///////////////////////////


void mouseMotion(int x, int y) {
	
	int differencex = x - lastx; // updates the x position to mouse movement
	lastx = x;
	int differencey = y - lasty; // updates the y position to mouse movement
	lasty = y;
	
	xrot += (float)  differencey; // adds difference between old and new x position to the rotation on the x-axis
	yrot += (float)  differencex; // adds difference between old and new y position to the rotation on the y-axis
	glutPostRedisplay(); 

}

//////////////////////////////
//          Camera          //
//////////////////////////////

void camera() {
	
	if (keyW) {
		
		float xrotrad, yrotrad;
		
		yrotrad = (yrot / 180 * 3.141592654f);
		xrotrad = (xrot / 180 * 3.141592654f);
		xpos += (float)(sin(yrotrad));
		zpos -= (float)(cos(yrotrad));
		ypos -= (float)(sin(xrotrad));
		glutPostRedisplay();
		
	}
	
	if (keyA) {
		
		float yrotrad;
		yrotrad = (yrot / 180 * 3.141592654f);
		xpos -= (float)(cos(yrotrad)) * 0.4;
		zpos -= (float)(sin(yrotrad)) * 0.4;
		glutPostRedisplay();
		
	}
	
	if (keyS) {
		
		float xrotrad, yrotrad;
		yrotrad = (yrot / 180 * 3.141592654f);
		xrotrad = (xrot / 180 * 3.141592654f);
		xpos -= (float)(sin(yrotrad));
		zpos += (float)(cos(yrotrad));
		ypos += (float)(sin(xrotrad));
		glutPostRedisplay();
		
	}
	
	if (keyD) {
		
		float yrotrad;
		yrotrad = (yrot / 180 * 3.141592654f);
		xpos += (float)(cos(yrotrad)) * 0.4;
		zpos += (float)(sin(yrotrad)) * 0.4;
		glutPostRedisplay();
		
	}
	
	// displays the whole movement with keyboard and mouse
	glRotatef(xrot,1.0,0.0,0.0); // rotates the camera on the x-axis
    glRotatef(yrot,0.0,1.0,0.0); // rotates the camera on the y-axis
	glTranslatef(-xpos, -ypos, -zpos); // sets the camera to the right position when moving around with WASD
}

//////////////////////////////
//      Display Callback    //
//////////////////////////////

void display() {
	
	
	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// Reset transformations
	glLoadIdentity();
	
	//Camera
	camera();
	glRotatef(rotationX, 0, 1, 0);

	//Light position
	GLfloat light_position[] = { 0.0f, 0.0f, 0.0f, 0.1f };
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	
	//glEnable(GL_LIGHTING);

	//skybox as displaylist
	generateSkyBox(16384,16384,16384);
	
	//glDisable(GL_LIGHTING);
	
	// Enable/Disable features
	glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		//glDisable(GL_LIGHTING);
		glDisable(GL_BLEND);
		glDisable(GL_DEPTH_TEST);
		glCallList(list);
	glPopAttrib();
	
	
	//Set Times
	cloudspeed = inc - 0.000777;
	cloud += cloudspeed;
	hour += inc;
	day += inc/24.0;
	
	glColor3f(1.0, 1.0, 1.0);
	 
	// sun
	glPushMatrix();
		glRotatef(360*day/365.0, 0.0, 1.0, 0.0);
		glDisable(GL_LIGHTING);    
		createSphere(20, 30, tex_planets[0]);
		glEnable(GL_LIGHTING);
	glPopMatrix();
	
	//merkur
	glPushMatrix();
		glRotatef(360.0*day/88.0, 0.0, 1.0, 0.0);
		glTranslatef(41.0, 0.0, 0.0);
		glRotatef(360.0*day/175.0, 0.0, 1.0, 0.0);
		createSphere(0.35, 30, tex_planets[1]);
	glPopMatrix();
	
	//Venus
	glPushMatrix();
		glRotatef(360.0*day/224.0, 0.0, 1.0, 0.0);
		glTranslatef(77.0, 0.0, 0.0);
		glRotatef(360.0*day/-116.0, 0.0, 1.0, 0.0);
		createSphere(0.86, 30, tex_planets[2]);
	glPopMatrix();
	
	//Earth
	glPushMatrix();
		glRotatef(360.0*day/365.0, 0.0, 1.0, 0.0);
		glTranslatef(107.0, 0.0, 0.0);
		glPushMatrix();
		glRotatef(360.0*hour/24.0, 0.0, 1.0, 0.0);
		createSphere(0.91, 30, tex_planets[3]);
	
		// moon
		glRotatef(360.0*4*day/365.0, 0.0, 1.0, 0.0);
		glTranslatef(3, 0.0f, 0.0f);
		glRotatef(360.0*4*day/365.0, 0.0, 1.0, 0.0);	 
		createSphere(0.25, 30, tex_planets[5]);
	
		glPopMatrix();
			glRotatef(360.0*cloud/24.0, 0.0, 1.0, 0.0);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
			createSphere(0.92, 30, tex_planets[4]);
			glDisable(GL_BLEND);
	
	glPopMatrix();
	
	
	//Mars
	glPushMatrix();
		glRotatef(360.0*day/686.0, 0.0, 1.0, 0.0);
		glTranslatef(163.0, 0.0, 0.0);
		glRotatef(360.0*hour/24.0, 0.0, 1.0, 0.0);
		createSphere(0.49, 30, tex_planets[6]);
		glPushMatrix();
	
			//phobos
			glRotatef(360.0*2*day/365.0, 0.0, 1.0, 0.0);
			glTranslatef(0.67, 0.0f, 0.0f);
			glRotatef(360.0*4*day/365.0, 0.0, 1.0, 0.0);	 
			createSphere(0.05, 30, tex_moons[0]);
	
		glPopMatrix();
		glPushMatrix();
	
			//deimos
			glRotatef(360.0*5*day/365.0, 0.0, 1.0, 0.0);
			glTranslatef(1.68, 0.0f, 0.0f);
			glRotatef(360.0*4*day/365.0, 0.0, 1.0, 0.0);	 
			createSphere(0.05, 30, tex_moons[1]);
		glPopMatrix();
	
	glPopMatrix();
	
	//Jupiter
	glPushMatrix();
		glRotatef(360.0*day/4332.0, 0.0, 1.0, 0.0);
		glTranslatef(400.0, 0.0, 0.0);
		glRotatef(360.0*hour/9.0, 0.0, 1.0, 0.0);
		createSphere(10.2, 30, tex_planets[7]);
		glPushMatrix();
	
			//Io
			glRotatef(360.0*2*day/1769.0, 0.0, 1.0, 0.0);
			glTranslatef(15, 0.0f, 0.0f);
			glRotatef(360.0*4*day/365.0, 0.0, 1.0, 0.0);	 
			createSphere(0.26, 30, tex_moons[2]);
	
		glPopMatrix();
		glPushMatrix();
	
			//Europa
			glRotatef(360.0*2*day/3551.0, 0.0, 1.0, 0.0);
			glTranslatef(20, 0.0f, 0.0f);
			glRotatef(360.0*4*day/365.0, 0.0, 1.0, 0.0);	 
			createSphere(0.22, 30, tex_moons[3]);
	
		glPopMatrix();
		glPushMatrix();
		
			//Ganymede
			glRotatef(360.0*2*day/7551.0, 0.0, 1.0, 0.0);
			glTranslatef(25, 0.0f, 0.0f);
			glRotatef(360.0*4*day/365.0, 0.0, 1.0, 0.0);	 
			createSphere(0.38, 30, tex_moons[4]);

		glPopMatrix();
		glPushMatrix();
	
			//Kallisto
			glRotatef(360.0*2*day/16689.0, 0.0, 1.0, 0.0);
			glTranslatef(30, 0.0f, 0.0f);
			glRotatef(360.0*4*day/365.0, 0.0, 1.0, 0.0);	 
			createSphere(0.35, 30, tex_moons[5]);
		glPopMatrix();
	
	glPopMatrix();
	
	//Saturn
	glPushMatrix();
		glRotatef(360.0*day/10759.0, 0.0, 1.0, 0.0);
		glTranslatef(600.0, 0.0, 0.0);
		glRotatef(360.0*hour/10.0, 0.0, 1.0, 0.0);
		createSphere(8.6, 30, tex_planets[8]);
	glPopMatrix();
	
	//Uranus
	glPushMatrix();
		glRotatef(360.0*day/30685.0, 0.0, 1.0, 0.0);
		glTranslatef(800.0, 0.0, 0.0);
		glRotatef(360.0*hour/-17.0, 0.0, 1.0, 0.0);
		createSphere(3.7, 30, tex_planets[9]);
	glPopMatrix();

	//Neptun
	glPushMatrix();
		glRotatef(360.0*day/60189.0, 0.0, 1.0, 0.0);
		glTranslatef(1050.0, 0.0, 0.0);
		glRotatef(360.0*hour/16.0, 0.0, 1.0, 0.0);
		createSphere(3.5, 30, tex_planets[10]);
	glPopMatrix();
	
	//Pluto
	glPushMatrix();
		glRotatef(360.0*day/90465.0, 0.0, 1.0, 0.0);
		glTranslatef(1200.0, 0.0, 0.0);
		glRotatef(360.0*hour/-6.0, 0.0, 1.0, 0.0);
		createSphere(0.16, 30, tex_planets[11]);
	glPopMatrix();
	
	glutSwapBuffers();    
}

//////////////////////////////
//       Paint Functions    //
//////////////////////////////


void generateSkyBox(int pWidth, int pHeight, int pLength) {
	
	float px,py,pz;
	list = glGenLists(1);
	
	px = - pWidth  / 2;
	py = - pHeight / 2;
	pz = - pLength / 2;

	glNewList ( list, GL_COMPILE );
	
	// Render the back quad
    glBindTexture(GL_TEXTURE_2D, tex_skybox[0]);
    glBegin(GL_QUADS);
		glTexCoord2f(0, 0); glVertex3f(px,          py,           pz);
		glTexCoord2f(0, 1); glVertex3f(px,          py + pHeight, pz);
		glTexCoord2f(1, 1); glVertex3f(px + pWidth, py + pHeight, pz);
		glTexCoord2f(1, 0); glVertex3f(px + pWidth, py,           pz);
    glEnd();
	
	// Render the front quad
    glBindTexture(GL_TEXTURE_2D, tex_skybox[1]);
    glBegin(GL_QUADS);
		glTexCoord2f(1, 0); glVertex3f(px,          py,           pz + pLength);
		glTexCoord2f(1, 1); glVertex3f(px,          py + pHeight, pz + pLength);
		glTexCoord2f(0, 1); glVertex3f(px + pWidth, py + pHeight, pz + pLength);
		glTexCoord2f(0, 0); glVertex3f(px + pWidth, py,           pz + pLength);
    glEnd();

	// Render the bottom quad
    glBindTexture(GL_TEXTURE_2D, tex_skybox[2]);
    glBegin(GL_QUADS);
		glTexCoord2f(1, 1); glVertex3f(px + pWidth, py, pz);
		glTexCoord2f(1, 0); glVertex3f(px + pWidth, py, pz + pLength);
		glTexCoord2f(0, 0); glVertex3f(px,          py, pz + pLength);
		glTexCoord2f(0, 1); glVertex3f(px,          py, pz);
    glEnd();
	
	// Render the top quad
    glBindTexture(GL_TEXTURE_2D, tex_skybox[3]);
    glBegin(GL_QUADS);
		glTexCoord2f(0, 1); glVertex3f(px,          py + pHeight, pz);
		glTexCoord2f(0, 0); glVertex3f(px,          py + pHeight, pz + pLength);
		glTexCoord2f(1, 0); glVertex3f(px + pWidth, py + pHeight, pz + pLength);
		glTexCoord2f(1, 1); glVertex3f(px + pWidth, py + pHeight, pz);
    glEnd();
	
	// Render the left quad
    glBindTexture(GL_TEXTURE_2D, tex_skybox[4]);
    glBegin(GL_QUADS);
		glTexCoord2f(0, 0); glVertex3f(px, py,           pz);
		glTexCoord2f(1, 0); glVertex3f(px, py,           pz + pLength);
		glTexCoord2f(1, 1); glVertex3f(px, py + pHeight, pz + pLength);
		glTexCoord2f(0, 1); glVertex3f(px, py + pHeight, pz);
    glEnd();
	
	// Render the right quad
    glBindTexture(GL_TEXTURE_2D, tex_skybox[5]);
    glBegin(GL_QUADS);
		glTexCoord2f(1, 0); glVertex3f(px + pWidth, py,           pz);
		glTexCoord2f(0, 0); glVertex3f(px + pWidth, py,           pz + pLength);
		glTexCoord2f(0, 1); glVertex3f(px + pWidth, py + pHeight, pz + pLength);
		glTexCoord2f(1, 1); glVertex3f(px + pWidth, py + pHeight, pz);
    glEnd();
	
    glEndList();
}

// From http://paulbourke.net/texture_colour/texturemap/
/*
 Create a sphere centered at c, with radius r, and precision n
 Draw a point for zero radius spheres
 */
void createSphere(double r,int n, GLuint texture) {
	int i,j;
	double theta1,theta2,theta3;
	XYZ e,p;
	XYZ c = {0, 0, 0};
	
	glBindTexture(GL_TEXTURE_2D, texture);  
	
	if (r < 0)
		r = -r;
	if (n < 0)
		n = -n;
	if (n < 4 || r <= 0) {
		glBegin(GL_POINTS);
		glVertex3f(c.x,c.y,c.z);
		glEnd();
		return;
	}
	
	for (j=0;j<n/2;j++) {
		theta1 = j * TWOPI / n - PID2;
		theta2 = (j + 1) * TWOPI / n - PID2;
		
		glBegin(GL_QUAD_STRIP);
		for (i=0;i<=n;i++) {
			theta3 = i * TWOPI / n;
			
			e.x = cos(theta2) * cos(theta3);
			e.y = sin(theta2);
			e.z = cos(theta2) * sin(theta3);
			p.x = c.x + r * e.x;
			p.y = c.y + r * e.y;
			p.z = c.z + r * e.z;
			
			glNormal3f(e.x,e.y,e.z);
			glTexCoord2f(-i/(double)n,-2*(j+1)/(double)n);
			glVertex3f(p.x,p.y,p.z);
			
			e.x = cos(theta1) * cos(theta3);
			e.y = sin(theta1);
			e.z = cos(theta1) * sin(theta3);
			p.x = c.x + r * e.x;
			p.y = c.y + r * e.y;
			p.z = c.z + r * e.z;
			
			glNormal3f(e.x,e.y,e.z);
			glTexCoord2f(-i/(double)n,-2*j/(double)n);
			glVertex3f(p.x,p.y,p.z);
		}
		glEnd();
	}
}

